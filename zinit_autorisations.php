<?php

/**
 * Définit les autorisations du plugin Initialiser Zcore.
 *
 * @plugin     Initialiser Zcore
 *
 * @copyright  2015-2025
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\Zinit\Autorisations
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline.
 *
 * @pipeline autoriser
 */
function zinit_autoriser() {
}
