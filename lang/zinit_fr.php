<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'appel_inclure_titre' => 'Utilisation de &lt;INCLURE&gt; et/ou #INCLURE',
	'appel_pages_titre' => 'Squelettes présents',

	// C
	'check_body_skel' => 'Le fichier <code>body.html</code> utilisé :',
	'check_liste_repertoires_skel_a_creer' => 'Les répertoires n\'ont pas encore été créés.',
	'check_liste_repertoires_skel' => 'Liste des répertoires créés par le plugin&nbsp;:',
	'check_presence_globale_z_blocs' => 'Est-ce que la globale "z_blocs" a été créée ?',
	'check_repertoire_skel_modifiable' => 'Le répertoire "squelettes" est-il accessible en écriture ?',
	'check_structure_skel' => 'Le fichier <code>structure.html</code> utilisé :',
	'copy_confirm_fichiers' => 'Désirez-vous copier les fichiers de \'squelettes_zcore\' vers \'squelettes\' ?',
	'copy_confirm_repertoires' => 'Désirez-vous copier les répertoires de \'squelettes_zcore\' vers \'squelettes\' ?',
	'copy_label' => 'Copier',

	// I
	'inclure_code' => 'Code',
	'inclure_nom' => 'Fond utilisé&nbsp;:',

	// M
	'maj_confirm_fichiers' => 'Désirez-vous mettre à jour les fichiers ?',
	'maj_confirm_repertoires' => 'Désirez-vous mettre à jour les répertoires ?',
	'maj_label' => 'Mise à jour',

	// P
	'page_nom' => 'Page&nbsp;:',
	'pages_nom' => 'Pages&nbsp;:',

	// S
	'surcharge_fichiers_dist' => 'Surcharger les fichiers de @repertoire@',

	// T
	'titre_blocs' => 'Blocs',
	'titre_controle' => 'Contrôle',
	'titre_objets' => 'Objets éditoriaux',
	'titre_page_zcore_skel' => 'Initialiser ses squelettes Zcore',
	'titre_repertoires' => 'Répertoires',
	'titre_verification' => 'Vérification',
	'titre_zcore_skel' => 'Squelettes Zcore',

	// Z
	'zinit_titre' => 'Initialiser Zcore',
);
