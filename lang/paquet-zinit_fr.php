<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// Z
	'zinit_description' => 'Ce plugin va vous permettre de commencer sereinement vos squelettes basés sous l\'architecture "Z" grâce au plugin Zcore. Il va créer dans  un répertoire les fichiers minimum pour créer vos squelettes.',
	'zinit_nom' => 'Initialiser Zcore',
	'zinit_slogan' => 'Bien commencer ses projets Zcore!',
);
