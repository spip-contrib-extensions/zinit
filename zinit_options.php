<?php

/**
 * Options du plugin Initialiser Zcore au chargement.
 *
 * @plugin     Initialiser Zcore
 *
 * @copyright  2015-2025
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\Zinit\Options
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
if (!isset($GLOBALS['zinit_tables_exclues'])) {
	$GLOBALS['zinit_tables_exclues'] = array(
		'spip_jobs',
		'spip_types_documents',
		'spip_messages',
		'spip_depots',
		'spip_plugins',
		'spip_paquets',
	);
}

if (!defined('_ZINIT_DIR_SQUELETTES')) {
	define('_ZINIT_DIR_SQUELETTES', _DIR_RACINE . 'squelettes_zcore/');
}
if (!defined('_DIR_SQUELETTES')) {
	define('_DIR_SQUELETTES', _DIR_RACINE . $GLOBALS['dossier_squelettes'] . '/');
}
if (!defined('_DIR_SQUELETTES_DIST')) {
	define('_DIR_SQUELETTES_DIST', _DIR_RACINE . 'squelettes-dist/');
}
