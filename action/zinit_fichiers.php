<?php

/**
 * Actions pour créer les fichiers squelettes des objets
 *
 * @plugin     Initialiser Zcore
 *
 * @copyright  2015-2025
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\Zinit\Action
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_zinit_fichiers_dist($arg = null) {
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	$arg = explode('/', $arg);
	list($webmestre, $repertoire) = $arg;
	if ($webmestre == 'oui') {
		include_spip('zinit_fonctions');
		if (!empty($repertoire) and $repertoire == 'dir_squelettes') {
			$cible = zinit_dossier_squelettes();
		} else {
			$cible = _ZINIT_DIR_SQUELETTES;
		}
		spip_log(print_r($arg, true), 'zinit');
		// On crée d'abord les répertoires
		$repertoires = zinit_repertoire_skel_creer($cible);
		// On crée les fichiers pour chaque objet
		$fichiers = zinit_template_skel_creer($cible);

		if ($repertoires and $fichiers) {
			if (!$redirect = _request('redirect')) {
				$redirect = parametre_url(generer_url_ecrire('zcore_skel'),'');
			}
			$redirect = str_replace('&amp;', '&', urldecode($redirect));
			include_spip('inc/headers');
			redirige_par_entete($redirect);
		}
	}

	return false;
}
