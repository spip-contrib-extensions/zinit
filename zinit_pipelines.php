<?php

/**
 * Utilisations de pipelines par Initialiser Zcore.
 *
 * @plugin     Initialiser Zcore
 *
 * @copyright  2015-2025
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\Zinit\Pipelines
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
